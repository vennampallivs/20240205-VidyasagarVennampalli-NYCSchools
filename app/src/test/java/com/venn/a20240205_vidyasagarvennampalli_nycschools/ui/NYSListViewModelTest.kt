package com.venn.a20240205_vidyasagarvennampalli_nycschools.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.data.NYCSchoolsRepository
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response.NYCSchoolsResponse
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response.SchoolsResponse
import com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschools.NYSListViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import kotlin.coroutines.ContinuationInterceptor

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner.Silent::class)
class NYSListViewModelTest: TestCoroutineScope by TestCoroutineScope(){

    @Mock
    private lateinit var repository: NYCSchoolsRepository

    @Mock
    private lateinit var service: NYCSchoolsRepository.Service

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp(){
        Dispatchers.setMain(this.coroutineContext[ContinuationInterceptor] as CoroutineDispatcher)
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testLoadingState(){

       runBlocking {
           Mockito.`when`(service.getNYCSchools()).thenReturn(NYCSchoolsResponse().apply {
               add(SchoolsResponse("123", "School One"))
               add(SchoolsResponse("345", "School Two"))
           })
           val viewModel = NYSListViewModel(repository)
           val test = viewModel.state
           with(test){
               assert(isLoading)
           }
       }
    }

    @Test
    fun testLoadedState(){
        runBlocking {
            Mockito.`when`(service.getNYCSchools()).thenReturn(NYCSchoolsResponse().apply {
                add(SchoolsResponse("567", "School Four"))
                add(SchoolsResponse("678", "School Five"))
            })
            val viewModel = NYSListViewModel(repository)
            val test = viewModel.state.copy(isLoading = false)
            with(test){
                assert(!isLoading)
            }
        }
    }
}