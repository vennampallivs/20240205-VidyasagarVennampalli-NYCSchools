package com.venn.a20240205_vidyasagarvennampalli_nycschools.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.venn.a20240205_vidyasagarvennampalli_nycschools.NavigationRoute
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.data.NYCSchoolsRepository
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response.SchoolDetailItem
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response.SchoolDetailResponse
import com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschool_details.NYSchoolDetailsViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner
import kotlin.coroutines.ContinuationInterceptor

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner.Silent::class)
class NYSchoolDetailsViewModelTest : TestCoroutineScope by TestCoroutineScope() {

    @Mock
    private lateinit var repository: NYCSchoolsRepository

    @Mock
    private lateinit var service: NYCSchoolsRepository.Service

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        Dispatchers.setMain(this.coroutineContext[ContinuationInterceptor] as CoroutineDispatcher)
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testSuccessfulFeed() {
        runBlocking {
            Mockito.`when`(service.getNYCSchoolDetails("123"))
                .thenReturn(SchoolDetailResponse().apply {
                    add(SchoolDetailItem("123", "School One", "67.8", "300.00", "283.00"))
                })

            val stateHandle = SavedStateHandle()
            stateHandle[NavigationRoute.Route.DBN_ID] = "123"

            val viewModel = NYSchoolDetailsViewModel(stateHandle, repository)

            val test = viewModel.state.copy(
                schoolDetailsItem = SchoolDetailItem(
                    "123",
                    "School One",
                    "67.8",
                    "300.00",
                    "283.00"
                )
            )
            with(test) {
                assert(schoolDetailsItem != null)
            }
        }
    }

    @Test
    fun testFailureFeed() {
        runBlocking {
            Mockito.`when`(service.getNYCSchoolDetails("123"))
                .thenReturn(null)

            val stateHandle = SavedStateHandle()
            stateHandle[NavigationRoute.Route.DBN_ID] = "XYZ"

            val viewModel = NYSchoolDetailsViewModel(stateHandle, repository)

            val test = viewModel.state.copy(
                schoolDetailsItem = null
            )
            with(test) {
                assert(schoolDetailsItem == null)
            }
        }
    }
}

