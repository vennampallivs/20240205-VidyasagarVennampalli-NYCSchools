package com.venn.a20240205_vidyasagarvennampalli_nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NYCApplication : Application() {
}