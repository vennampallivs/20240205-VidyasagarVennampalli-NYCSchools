package com.venn.a20240205_vidyasagarvennampalli_nycschools.di

import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.data.NYCSchoolsRepository
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.data.NYCSchoolsRepository.Companion.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NYCModule {

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient
            .Builder()
            .build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit
            .Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()
    }

    @Provides
    @Singleton
    fun provideService(
        retrofit: Retrofit
    ): NYCSchoolsRepository.Service {
        return retrofit.create(NYCSchoolsRepository.Service::class.java)
    }
}