package com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response

import com.google.gson.annotations.SerializedName

class NYCSchoolsResponse : ArrayList<SchoolsResponse>()

class SchoolDetailResponse : ArrayList<SchoolDetailItem>()

data class SchoolsResponse(
    @SerializedName("dbn") val dbn: String,
    @SerializedName("school_name") val name: String
)

data class SchoolDetailItem(
    @SerializedName("dbn") val dbn: String,
    @SerializedName("school_name") val name: String,
    @SerializedName("sat_critical_reading_avg_score") val readingScore: String,
    @SerializedName("sat_math_avg_score") val mathScore: String,
    @SerializedName("sat_writing_avg_score") val writingScore: String,
)
