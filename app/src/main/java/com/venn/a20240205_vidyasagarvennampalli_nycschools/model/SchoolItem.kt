package com.venn.a20240205_vidyasagarvennampalli_nycschools.model

data class SchoolItem(val dbn: String, val name: String)

data class SchoolDetailsItem(
    val dbn: String, val name: String,
    val mathScore: String, val readingScore: String, val writingScore: String
)

