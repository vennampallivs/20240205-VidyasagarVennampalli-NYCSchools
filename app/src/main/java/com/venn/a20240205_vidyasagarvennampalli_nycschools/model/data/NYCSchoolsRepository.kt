package com.venn.a20240205_vidyasagarvennampalli_nycschools.model.data

import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.SchoolDetailsItem
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.SchoolItem
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response.NYCSchoolsResponse
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response.SchoolDetailResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class NYCSchoolsRepository @Inject constructor(private val service: Service) {

    private var cachedSchoolItems: List<SchoolItem>? = null

    suspend fun getNYCSchools(): List<SchoolItem> = withContext(Dispatchers.IO) {
        var cachedSchoolItems = cachedSchoolItems
        if (cachedSchoolItems == null) {
            cachedSchoolItems = service.getNYCSchools().mapSchoolsToItems()
            this@NYCSchoolsRepository.cachedSchoolItems = cachedSchoolItems
        }
        return@withContext cachedSchoolItems
    }

    suspend fun getNYCSchoolDetails(dbn: String): SchoolDetailResponse? =
        withContext(Dispatchers.IO) {
            return@withContext service.getNYCSchoolDetails(dbn)
        }

    interface Service {

        @GET("resource/s3k6-pzi2.json")
        suspend fun getNYCSchools(): NYCSchoolsResponse

        @GET("resource/f9bf-2cp4.json")
        suspend fun getNYCSchoolDetails(@Query("dbn") dbn: String): SchoolDetailResponse
    }


    private fun NYCSchoolsResponse.mapSchoolsToItems(): List<SchoolItem> {

        return this.map { school ->
            SchoolItem(
                dbn = school.dbn,
                name = school.name
            )
        }
    }

    private fun SchoolDetailResponse.mapSchoolsDetailToItem(): SchoolDetailsItem? {

        return if (this.size > 1) {
            val obj = this[0].let {
                SchoolDetailsItem(
                    it.dbn,
                    it.name,
                    it.mathScore,
                    it.readingScore,
                    it.writingScore
                )
            }
            obj
        } else {
            null
        }
    }

    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/"
    }
}