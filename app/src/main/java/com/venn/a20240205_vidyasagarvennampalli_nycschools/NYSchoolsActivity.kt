package com.venn.a20240205_vidyasagarvennampalli_nycschools

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschool_details.NYSchoolDetailsScreen
import com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschool_details.NYSchoolDetailsViewModel
import com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschools.NYSListViewModel
import com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschools.SchoolsListScreen
import com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.theme.ComposableTheme
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.receiveAsFlow

@AndroidEntryPoint
class NYSchoolsActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposableTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize()
                ) {
                    NYSchoolMainScreen()
                }
            }
        }
    }
}

@Composable
private fun NYSchoolMainScreen() {
    val navController = rememberNavController()
    NavHost(navController, startDestination = NavigationRoute.Route.SCHOOLS_LIST) {
        composable(route = NavigationRoute.Route.SCHOOLS_LIST) {
            SchoolsListDestination(navController)
        }
        composable(
            route = NavigationRoute.Route.SCHOOL_DETAILS,
            arguments = listOf(navArgument(NavigationRoute.Route.DBN_ID) {
                type = NavType.StringType
            })
        ) {
            SchoolDetailsDestination(navController)
        }
    }

}

@Composable
fun SchoolsListDestination(navController: NavHostController) {
    val viewModel: NYSListViewModel = hiltViewModel()
    val errorAlertDialog = remember { mutableStateOf(false) }
    errorAlertDialog.value = false
    SchoolsListScreen(
        navController,
        state = viewModel.state,
        effectFlow = viewModel.effects.receiveAsFlow(),
        onNavigationRequested = { dbn ->
            navController.navigate("${NavigationRoute.Route.SCHOOLS_LIST}/${dbn}")
        }
    ) {
        viewModel.retry()
    }
}

@Composable
fun SchoolDetailsDestination(navController: NavHostController) {
    val viewModel: NYSchoolDetailsViewModel = hiltViewModel()
    NYSchoolDetailsScreen(state = viewModel.state, navController)
}

object NavigationRoute {

    object Route {
        const val DBN_ID = "DBN"
        const val SCHOOLS_LIST = "schools_list"
        const val SCHOOL_DETAILS = "$SCHOOLS_LIST/{$DBN_ID}"
    }
}
