package com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschool_details

import android.annotation.SuppressLint
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.venn.a20240205_vidyasagarvennampalli_nycschools.R
import com.venn.a20240205_vidyasagarvennampalli_nycschools.extensions.showDialog
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response.SchoolDetailItem

@OptIn(ExperimentalMaterial3Api::class)
@SuppressLint("UnusedMaterialScaffoldPaddingParameter", "UnusedMaterial3ScaffoldPaddingParameter")
@Composable
fun NYSchoolDetailsScreen(state: NYCSchoolDetailsContract.State, navController: NavHostController) {
    val errorAlertDialog = remember { mutableStateOf(false) }
    errorAlertDialog.value = false
    Surface(color = MaterialTheme.colors.surface) {
        Scaffold(
            topBar = {
                showAppBar(navController)
            }
        ) { innerPadding ->
            Modifier.padding(innerPadding)
            Box(
                modifier = Modifier
                    .padding(top = 48.dp)
                    .fillMaxSize()
                    .background(color = MaterialTheme.colors.background)
            ) {

                Column(
                    modifier = Modifier
                        .animateContentSize()
                        .fillMaxWidth()
                        .padding(
                            start = 8.dp,
                            end = 8.dp,
                            top = 16.dp,
                            bottom = 16.dp
                        )
                ) {
                    state?.schoolDetailsItem?.let {
                        SchoolNameDisplay(state?.schoolDetailsItem?.name ?: "", modifier = Modifier)
                        Spacer(modifier = Modifier.height(2.dp))
                        SchoolDetails(state.schoolDetailsItem)
                        Spacer(modifier = Modifier.height(8.dp))
                        BackButtonDisplay(
                            name = stringResource(R.string.back),
                            modifier = Modifier,
                            navController
                        )
                    } ?: showDialog(
                        navController,
                        errorAlertDialog,
                        "Error Occurred!",
                        "No info for selected school available.",
                        null
                    )

                }

            }
        }
    }
}


@Composable
fun showAppBar(navController: NavHostController) {
    TopAppBar(
        navigationIcon = {
            IconButton(
                onClick = {
                    navController.navigateUp()
                }
            ) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    modifier = Modifier.padding(horizontal = 12.dp),
                    contentDescription = "Action icon"
                )
            }

        },
        title = { Text(stringResource(R.string.school_details)) },
        backgroundColor = MaterialTheme.colors.background
    )
}

@Composable
private fun SchoolDetails(schoolDetailsItem: SchoolDetailItem?) {
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 2.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 8.dp, end = 8.dp, top = 8.dp)
    ) {
        Column(
            modifier = Modifier
                .animateContentSize()
                .fillMaxWidth()
                .padding(
                    start = 8.dp,
                    end = 8.dp,
                    top = 16.dp,
                    bottom = 16.dp
                )
        ) {
            SchoolDetailItemDisplay(
                stringResource(R.string.math_score),
                schoolDetailsItem?.mathScore ?: "",
                modifier = Modifier
            )
            Spacer(modifier = Modifier.height(2.dp))
            SchoolDetailItemDisplay(
                stringResource(R.string.reading_score),
                schoolDetailsItem?.readingScore ?: "",
                modifier = Modifier
            )
            Spacer(modifier = Modifier.height(2.dp))
            SchoolDetailItemDisplay(
                stringResource(R.string.writing_score),
                schoolDetailsItem?.writingScore ?: "",
                modifier = Modifier
            )

        }
    }
}


@Composable
fun SchoolDetailItemDisplay(
    label: String,
    value: String,
    modifier: Modifier
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .padding(8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {

        Column(modifier = Modifier.weight(1F), horizontalAlignment = Alignment.Start) {
            Text(
                text = label,
                textAlign = TextAlign.End,
                style = MaterialTheme.typography.h6,
            )
        }
        Column(modifier = Modifier.weight(1F), horizontalAlignment = Alignment.End) {
            Text(
                text = value,
                textAlign = TextAlign.Start,
                style = MaterialTheme.typography.h6
            )
        }

    }
}

@Composable
fun SchoolNameDisplay(
    name: String,
    modifier: Modifier

) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .padding(start = 8.dp, end = 8.dp, top = 8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            modifier = modifier.fillMaxWidth(),
            text = name,
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.h5
        )
    }
}

@Composable
fun BackButtonDisplay(
    name: String,
    modifier: Modifier,
    navController: NavHostController
) {
    Row(
        modifier = modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        ElevatedButton(
            onClick = {
                navController.navigateUp()
            },
            colors = ButtonDefaults.filledTonalButtonColors(containerColor = MaterialTheme.colors.surface),
            modifier = modifier
                .align(alignment = Alignment.CenterVertically)
                .fillMaxWidth()
        ) {
            Text(
                text = name,
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.h5
            )
        }
    }
}