package com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschools

import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.SchoolItem

class NYCSchoolsContract {

    data class State(
        val schools: List<SchoolItem>? = listOf(),
        val isLoading: Boolean = false,
        val errorMessage: String? = null
    )

    sealed class Effect {
        object Loaded : Effect()
    }
}