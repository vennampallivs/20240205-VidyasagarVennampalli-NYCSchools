package com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschool_details

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.venn.a20240205_vidyasagarvennampalli_nycschools.NavigationRoute
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.data.NYCSchoolsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
open class NYSchoolDetailsViewModel @Inject constructor(
    private val stateHandle: SavedStateHandle?,
    private val repository: NYCSchoolsRepository
) : ViewModel() {

    var state by mutableStateOf(
        NYCSchoolDetailsContract.State(
            null
        )
    )

    init {
        viewModelScope.launch {
            val dbn = stateHandle?.get<String>(NavigationRoute.Route.DBN_ID)
                ?: throw IllegalStateException("DBN has to pass to destination.")
            val schoolDetailsItem = repository.getNYCSchoolDetails(dbn)
            val size = schoolDetailsItem?.let {
                it.size
            } ?: 0
            if (size > 0) {
                viewModelScope.launch {
                    state = state.copy(schoolDetailsItem = schoolDetailsItem?.get(0))
                }
            } else {
                state = state.copy(null)
            }
        }
    }
}