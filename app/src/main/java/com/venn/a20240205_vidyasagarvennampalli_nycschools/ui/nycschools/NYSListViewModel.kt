package com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschools

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.data.NYCSchoolsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class NYSListViewModel @Inject constructor(private val repository: NYCSchoolsRepository) :
    ViewModel() {

    var state by mutableStateOf(
        NYCSchoolsContract.State(
            schools = listOf(),
            isLoading = true,
            errorMessage = null
        )
    )

    var effects = Channel<NYCSchoolsContract.Effect>(Channel.UNLIMITED)

    init {
        viewModelScope.launch { getNYCSchools() }
    }

    private suspend fun getNYCSchools() {
        try {
            val schools = repository.getNYCSchools()
            viewModelScope.launch {
                state = state.copy(schools = schools, isLoading = false)
                effects.send(NYCSchoolsContract.Effect.Loaded)
            }
        } catch (e: Exception){
            Log.i("NYSListViewModel","Exception= ${e.message}")
            state = state.copy(schools = null, isLoading = false, errorMessage = e.message)
        }
    }

    fun retry(){
        state = state.copy(schools = listOf(), isLoading = true)
        viewModelScope.launch { getNYCSchools() }
    }

}