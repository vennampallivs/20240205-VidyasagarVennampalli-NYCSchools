package com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschool_details

import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.response.SchoolDetailItem

class NYCSchoolDetailsContract {
    data class State(
        val schoolDetailsItem: SchoolDetailItem?
    )
}