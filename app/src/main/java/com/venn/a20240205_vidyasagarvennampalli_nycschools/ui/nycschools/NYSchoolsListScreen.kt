package com.venn.a20240205_vidyasagarvennampalli_nycschools.ui.nycschools

import android.annotation.SuppressLint
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.venn.a20240205_vidyasagarvennampalli_nycschools.R
import com.venn.a20240205_vidyasagarvennampalli_nycschools.extensions.showDialog
import com.venn.a20240205_vidyasagarvennampalli_nycschools.model.SchoolItem
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onEach

@SuppressLint("UnusedMaterialScaffoldPaddingParameter", "SuspiciousIndentation")
@Composable
fun SchoolsListScreen(
    navController: NavHostController,
    state: NYCSchoolsContract.State,
    effectFlow: Flow<NYCSchoolsContract.Effect>?,
    onNavigationRequested: (itemId: String) -> Unit,
    onError: () -> Unit
) {

    val errorAlertDialog = remember { mutableStateOf(false) }
    errorAlertDialog.value = false
    Surface(color = MaterialTheme.colors.surface) {
        //Text(text = "$count")

        val scaffoldState: ScaffoldState = rememberScaffoldState()
        // handle effects from viewmodel
        LaunchedEffect(effectFlow) {
            effectFlow?.onEach { effect ->
                if (effect is NYCSchoolsContract.Effect.Loaded)
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = "School list were loaded.",
                        duration = SnackbarDuration.Indefinite
                    )
            }?.collect()
        }
        Scaffold(
            scaffoldState = scaffoldState,
            topBar = {
                showAppBar()
            },
        ) { innerPadding ->
            Modifier.padding(innerPadding)
            Box(modifier = Modifier) {
                Column(
                    modifier = Modifier
                        .animateContentSize()
                        .fillMaxWidth()
                ) {
                    state.schools?.let {
                        SchoolsList(schools = state.schools) { itemId ->
                            onNavigationRequested(itemId)
                        }
                    } ?: showError(
                        navController,
                        errorAlertDialog = errorAlertDialog,
                        onError = onError,
                        state = state
                    )
                    if (state.isLoading)
                        LoadingBar()
                }

            }
        }
    }
}

@Composable
fun showError(
    navController: NavHostController,
    errorAlertDialog: MutableState<Boolean>,
    onError: () -> Unit,
    state: NYCSchoolsContract.State
) {
    val message = state.errorMessage ?: "Data Retrieve Failure."
    showDialog(
        navController = navController,
        errorAlertDialog = errorAlertDialog,
        title = "Error Occurred!",
        message = message,
        onConfirmRequest = {
            onError.invoke()
        }
    )

}

@Composable
fun SchoolsList(schools: List<SchoolItem>?, onItemClicked: (id: String) -> Unit = { }) {
    LazyColumn(
        contentPadding = PaddingValues(bottom = 16.dp)
    ) {
        items(schools!!) { school ->
            SchoolItemRow(school = school, onItemClicked = onItemClicked)
        }
    }
}

@Composable
fun SchoolItemRow(school: SchoolItem, onItemClicked: (id: String) -> Unit) {
    Card(
        shape = RoundedCornerShape(8.dp),
        elevation = 2.dp,
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 8.dp, end = 8.dp, top = 8.dp)
            .clickable { onItemClicked(school.dbn) }
    ) {
        Row(modifier = Modifier.animateContentSize()) {
            SchoolItemDisplay(
                item = school,
                modifier = Modifier
                    .padding(
                        start = 8.dp,
                        end = 8.dp,
                        top = 16.dp,
                        bottom = 16.dp
                    )
                    .fillMaxWidth(0.80f)
                    .align(Alignment.CenterVertically)
            )
        }
    }
}

@Composable
fun SchoolItemDisplay(
    item: SchoolItem?,
    modifier: Modifier
) {
    Column(modifier = modifier) {
        Text(
            text = item?.name ?: "",
            textAlign = TextAlign.Left,
            style = MaterialTheme.typography.h6,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis
        )
    }
}

@Composable
fun showAppBar() {
    TopAppBar(
        navigationIcon = {
            Icon(
                imageVector = Icons.Default.Home,
                modifier = Modifier.padding(horizontal = 12.dp),
                contentDescription = "Action icon"
            )
        },
        title = { Text(stringResource(R.string.app_name)) },
        backgroundColor = MaterialTheme.colors.background
    )
}

@Composable
fun LoadingBar() {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        CircularProgressIndicator()
    }
}