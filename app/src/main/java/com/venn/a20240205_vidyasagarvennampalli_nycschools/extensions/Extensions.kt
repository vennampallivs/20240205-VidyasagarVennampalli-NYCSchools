package com.venn.a20240205_vidyasagarvennampalli_nycschools.extensions

import androidx.compose.foundation.layout.padding
import androidx.compose.material.AlertDialog
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ErrorAlertDialog(
    onDismissRequest: () -> Unit,
    onConfirmRequest: (() -> Unit)?,
    dialogTitle: String,
    dialogText: String
) {
    AlertDialog(
        title = {
            Text(text = dialogTitle, fontSize = 18.sp, modifier = Modifier.padding(bottom = 16.dp))
        },
        text = {
            Text(text = dialogText, fontSize = 24.sp)
        },
        onDismissRequest = {
            onDismissRequest()
        },
        confirmButton = {
            onConfirmRequest?.let {
                TextButton(
                    onClick = {
                        onConfirmRequest()
                    }
                ) {
                    Text("Retry", fontSize = 18.sp)
                }
            }
        },
        dismissButton = {
            TextButton(
                onClick = {
                    onDismissRequest()
                }
            ) {
                Text("Dismiss", fontSize = 18.sp)
            }
        }
    )
}

@Composable
fun showDialog(
    navController: NavHostController?,
    errorAlertDialog: MutableState<Boolean>,
    title: String,
    message: String,
    onConfirmRequest: (() -> Unit)?,
) {
    errorAlertDialog.value = true
    showErrorDialog(navController, errorAlertDialog, title, message, onConfirmRequest)
}

@Composable
fun showErrorDialog(
    navController: NavHostController?, errorAlertDialog: MutableState<Boolean>,
    title: String, message: String, onConfirmRequest: (() -> Unit)?,
) {
    when {
        errorAlertDialog.value -> {
            ErrorAlertDialog(
                onDismissRequest = {
                    errorAlertDialog.value = false
                    navController?.navigateUp()
                },
                onConfirmRequest = onConfirmRequest?.let {
                    {
                        errorAlertDialog.value = false
                        onConfirmRequest?.invoke()
                    }
                } ?: null,
                dialogTitle = title,
                dialogText = message
            )
        }
    }
}